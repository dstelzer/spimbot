.data

.align 2
problem_stack:
#	.word 2 0 0 2 0 2 2 2 2 2 2 0 2 1 1 1 0 0 1 0 1 1 0 2 1 1 2 1 1 1 1 1 0 1 2 0 1 2 1 1 2 2
problem_stack_start:
	.space 4096

.text

WATER_TO_USE = 4					# How much water to use on a plant

.globl init_stack
init_stack:
	la	$t9,	problem_stack_start
	jr	$ra

.globl check_stack
check_stack:
	la	$t0,	problem_stack
	bgt	$t9,	$t0,	stack_not_empty
	jr	$ra							# Just return without doing anything
stack_not_empty:
	lw	$a2,	-12($t9)			# Type (0 for none, 1 for plant, 2 for water, 3 for fire, 4 for harvest, 5 for burn)
	lw	$a0,	-8($t9)				# X coordinate
	lw	$a1,	-4($t9)				# Y coordinate
	sub	$t9,	$t9,	12			# Finally pop - slightly safer to do this at the end rather than the beginning
	
	sub	$sp,	$sp,	4
	sw	$ra,	0($sp)
	
	jal	move_to						# Get there by any means necessary!
	
	lw	$ra,	0($sp)
	add	$sp,	$sp,	4
	
	beq	$a2,	1,		do_plant
	beq	$a2,	2,		do_water
	beq	$a2,	3,		do_fire
	beq	$a2,	4,		do_harvest
	beq	$a2,	5,		do_burn

stack_done:
	j	check_stack					# Tail recurse until stack is empty (at which point it'll return from the top)
	
do_plant:
	lw	$t0,	GET_NUM_SEEDS
	beq	$t0,	$zero,	stack_done
	sw	$zero,	SEED_TILE
	j	stack_done
do_water:
	lw	$t0,	GET_NUM_WATER_DROPS
	beq	$t0,	$zero,	stack_done
	li	$t0,	WATER_TO_USE
	sw	$t0,	WATER_TILE
	j	stack_done
do_fire:
	lw	$t0,	GET_NUM_WATER_DROPS
	beq	$t0,	$zero,	stack_done
	sw	$zero,	PUT_OUT_FIRE
	j	stack_done
do_harvest:
	sw	$zero,	HARVEST_TILE
	j	stack_done
do_burn:
	lw	$t0,	GET_NUM_FIRE_STARTERS
	beq	$t0,	$zero,	stack_done
	sw	$zero,	BURN_TILE
	j	stack_done

.text

SPEEDP = 10
SPEEDN = -10

.globl move_to
move_to:
	sw	$zero,	VELOCITY
	mul	$a0,	$a0,	30
	add	$a0,	$a0,	15
	mul	$a1,	$a1,	30
	add	$a1,	$a1,	15
	
	# First, in the X direction
	lw	$t0,	BOT_X
	sub	$t0,	$a0,	$t0
	
	sw	$zero,	ANGLE
	li	$t2,	1
	sw	$t2,	ANGLE_CONTROL
	
	beq	$t0,	$zero,	xdone
	blt	$t0,	$zero,	xneg
	j	xpos

xpos:
	li	$t2,	SPEEDP
	sw	$t2,	VELOCITY
	sub	$a0,	$a0,	5
xpr:
	lw	$t0,	BOT_X
	ble	$t0,	$a0,	xpr
	sw	$zero,	VELOCITY
	j	xdone
	
xneg:
	li	$t2,	SPEEDN
	sw	$t2,	VELOCITY
	add	$a0,	$a0,	5
xnr:
	lw	$t0,	BOT_X
	bge	$t0,	$a0,	xnr
	sw	$zero,	VELOCITY
	j	xdone

xdone:
	
	# Now, for Y
	lw	$t0,	BOT_Y
	sub	$t0,	$a1,	$t0
	
	li	$t2,	90
	sw	$t2,	ANGLE
	sw	$zero,	ANGLE_CONTROL
	
	beq	$t0,	$zero,	ydone
	blt	$t0,	$zero,	yneg
	j	ypos

ypos:
	li	$t2,	SPEEDP
	sw	$t2,	VELOCITY
	sub	$a1,	$a1,	5
ypr:
	lw	$t0,	BOT_Y
	ble	$t0,	$a1,	ypr
	sw	$zero,	VELOCITY
	j	ydone
	
yneg:
	li	$t2,	SPEEDN
	sw	$t2,	VELOCITY
	add	$a1,	$a1,	5
ynr:
	lw	$t0,	BOT_Y
	bge	$t0,	$a1,	ynr
	sw	$zero,	VELOCITY
	j	ydone

ydone:
	jr	$ra
