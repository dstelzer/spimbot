Strategy suggestion:

Pick a patch of nine tiles (away from other bot)
Request seeds
Request water
Plant seeds on all
Water them
Lurk there

Have another "stack" (reserve a high-numbered t-register as a stack pointer)
On FIRE or MAX_GROWTH, add that tile to the stack
Whenever the stack is not empty, go to the tile, put out the fire or harvest it

Does this sound good?
I can handle adapting the puzzle-request and puzzle-solving code, and the movement based on the stack, if you can take the interrupt handler and finding a good set of tiles?
