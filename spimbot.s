# syscall constants
PRINT_STRING = 4
PRINT_CHAR   = 11
PRINT_INT    = 1

# debug constants
PRINT_INT_ADDR   = 0xffff0080
PRINT_FLOAT_ADDR = 0xffff0084
PRINT_HEX_ADDR   = 0xffff0088

# spimbot constants
VELOCITY       = 0xffff0010
ANGLE          = 0xffff0014
ANGLE_CONTROL  = 0xffff0018
BOT_X          = 0xffff0020
BOT_Y          = 0xffff0024
OTHER_BOT_X    = 0xffff00a0
OTHER_BOT_Y    = 0xffff00a4
TIMER          = 0xffff001c
SCORES_REQUEST = 0xffff1018

TILE_SCAN       = 0xffff0024
SEED_TILE       = 0xffff0054
WATER_TILE      = 0xffff002c
MAX_GROWTH_TILE = 0xffff0030
HARVEST_TILE    = 0xffff0020
BURN_TILE       = 0xffff0058
GET_FIRE_LOC    = 0xffff0028
PUT_OUT_FIRE    = 0xffff0040

GET_NUM_WATER_DROPS   = 0xffff0044
GET_NUM_SEEDS         = 0xffff0048
GET_NUM_FIRE_STARTERS = 0xffff004c
SET_RESOURCE_TYPE     = 0xffff00dc
REQUEST_PUZZLE        = 0xffff00d0
SUBMIT_SOLUTION       = 0xffff00d4

# interrupt constants
BONK_MASK               = 0x1000
BONK_ACK                = 0xffff0060
TIMER_MASK              = 0x8000
TIMER_ACK               = 0xffff006c
ON_FIRE_MASK            = 0x400
ON_FIRE_ACK             = 0xffff0050
MAX_GROWTH_ACK          = 0xffff005c
MAX_GROWTH_INT_MASK     = 0x2000
REQUEST_PUZZLE_ACK      = 0xffff00d8
REQUEST_PUZZLE_INT_MASK = 0x800
.text

fix_interrupts:
	li	$t4,	ON_FIRE_MASK						# timer interrupt enable bit
	or	$t4,	$t4,	MAX_GROWTH_INT_MASK			# timer interrupt enable bit
	or	$t4,	$t4,	REQUEST_PUZZLE_INT_MASK		# timer interrupt enable bit
	or	$t4,	$t4,	1							# global interrupt enable
	mtc0	$t4, $12								# set interrupt mask (Status register)
	jr	$ra

.kdata				# interrupt handler data (separated just for readability)
.align		2
chunkIH:	.space 8	# space for two registers
non_intrpt_str:	.asciiz "Non-interrupt exception\n"
unhandled_str:	.asciiz "Unhandled interrupt type\n"

.ktext 0x80000180
interrupt_handler:
.set noat
	move	$k1, $at		# Save $at
.set at
	la	$k0, chunkIH
	sw	$a0, 0($k0)		# Get some free registers
	sw	$a1, 4($k0)		# by storing them to a global variable

	mfc0	$k0, $13		# Get Cause register
	srl	$a0, $k0, 2
	and	$a0, $a0, 0xf		# ExcCode field
	bne	$a0, 0, non_intrpt         

interrupt_dispatch:			# Interrupt
	mfc0	$k0, $13		# Get Cause register, again
	beq	$k0, 0, done		# handled all outstanding interrupts

	and	$a0, $k0, ON_FIRE_MASK
	bne	$a0, 0, fire_interrupt
	
	and	$a0, $k0, MAX_GROWTH_INT_MASK
	bne	$a0, 0, growth_interrupt
	
	and	$a0, $k0, REQUEST_PUZZLE_INT_MASK
	bne	$a0, 0, puzzle_interrupt

	li	$v0, PRINT_STRING	# Unhandled interrupt types
	la	$a0, unhandled_str
	syscall
	j	done

fire_interrupt:
	sw	$zero,	ON_FIRE_ACK		# acknowledge interrupt
	
	li	$a0,	3
	sw	$a0,	0($t9)

	lw	$a0,	GET_FIRE_LOC
	and	$a1,	$a0,	0xffff	# lower bits
	srl	$a0,	$a0,	16
	
	sw	$a0,	4($t9)
	sw	$a1,	8($t9)
	add	$t9,	$t9,	12

	j	interrupt_dispatch	# see if other interrupts are waiting

growth_interrupt:
	sw	$zero,	MAX_GROWTH_ACK		# acknowledge interrupt
	
	li	$a0,	4
	sw	$a0,	0($t9)

	lw	$a0,	MAX_GROWTH_TILE
	and	$a1,	$a0,	0xffff	# lower bits
	srl	$a0,	$a0,	16
	
	sw	$a0,	4($t9)
	sw	$a1,	8($t9)
	add	$t9,	$t9,	12

	j	interrupt_dispatch	# see if other interrupts are waiting

puzzle_interrupt:
	sw	$zero,	REQUEST_PUZZLE_ACK
	
	sw	$a0,	puzzle_ready
	j	interrupt_dispatch

non_intrpt:				# was some non-interrupt
	li	$v0, PRINT_STRING
	la	$a0, non_intrpt_str
	syscall				# print out an error message
	# fall through to done

done:
	la	$k0, chunkIH
	lw	$a0, 0($k0)		# Restore saved registers
	lw	$a1, 4($k0)
.set noat
	move	$at, $k1		# Restore $at
.set at 
	eret
.data

.align 2
problem_stack:
#	.word 2 0 0 2 0 2 2 2 2 2 2 0 2 1 1 1 0 0 1 0 1 1 0 2 1 1 2 1 1 1 1 1 0 1 2 0 1 2 1 1 2 2
problem_stack_start:
	.space 4096

.text

WATER_TO_USE = 4					# How much water to use on a plant

.globl init_stack
init_stack:
	la	$t9,	problem_stack_start
	jr	$ra

.globl check_stack
check_stack:
	la	$t0,	problem_stack
	bgt	$t9,	$t0,	stack_not_empty
	jr	$ra							# Just return without doing anything
stack_not_empty:
	lw	$a2,	-12($t9)			# Type (0 for none, 1 for plant, 2 for water, 3 for fire, 4 for harvest, 5 for burn)
	lw	$a0,	-8($t9)				# X coordinate
	lw	$a1,	-4($t9)				# Y coordinate
	sub	$t9,	$t9,	12			# Finally pop - slightly safer to do this at the end rather than the beginning
	
	sub	$sp,	$sp,	4
	sw	$ra,	0($sp)
	
	jal	move_to						# Get there by any means necessary!
	
	lw	$ra,	0($sp)
	add	$sp,	$sp,	4
	
	beq	$a2,	1,		do_plant
	beq	$a2,	2,		do_water
	beq	$a2,	3,		do_fire
	beq	$a2,	4,		do_harvest
	beq	$a2,	5,		do_burn

stack_done:
	j	check_stack					# Tail recurse until stack is empty (at which point it'll return from the top)
	
do_plant:
	lw	$t0,	GET_NUM_SEEDS
	beq	$t0,	$zero,	stack_done
	sw	$zero,	SEED_TILE
	j	stack_done
do_water:
	lw	$t0,	GET_NUM_WATER_DROPS
	beq	$t0,	$zero,	stack_done
	li	$t0,	WATER_TO_USE
	sw	$t0,	WATER_TILE
	j	stack_done
do_fire:
	lw	$t0,	GET_NUM_WATER_DROPS
	beq	$t0,	$zero,	stack_done
	sw	$zero,	PUT_OUT_FIRE
	j	stack_done
do_harvest:
	sw	$zero,	HARVEST_TILE
	j	stack_done
do_burn:
	lw	$t0,	GET_NUM_FIRE_STARTERS
	beq	$t0,	$zero,	stack_done
	sw	$zero,	BURN_TILE
	j	stack_done

.text

SPEEDP = 10
SPEEDN = -10

.globl move_to
move_to:
	sw	$zero,	VELOCITY
	mul	$a0,	$a0,	30
	add	$a0,	$a0,	15
	mul	$a1,	$a1,	30
	add	$a1,	$a1,	15
	
	# First, in the X direction
	lw	$t0,	BOT_X
	sub	$t0,	$a0,	$t0
	
	sw	$zero,	ANGLE
	li	$t2,	1
	sw	$t2,	ANGLE_CONTROL
	
	beq	$t0,	$zero,	xdone
	blt	$t0,	$zero,	xneg
	j	xpos

xpos:
	li	$t2,	SPEEDP
	sw	$t2,	VELOCITY
	sub	$a0,	$a0,	5
xpr:
	lw	$t0,	BOT_X
	ble	$t0,	$a0,	xpr
	sw	$zero,	VELOCITY
	j	xdone
	
xneg:
	li	$t2,	SPEEDN
	sw	$t2,	VELOCITY
	add	$a0,	$a0,	5
xnr:
	lw	$t0,	BOT_X
	bge	$t0,	$a0,	xnr
	sw	$zero,	VELOCITY
	j	xdone

xdone:
	
	# Now, for Y
	lw	$t0,	BOT_Y
	sub	$t0,	$a1,	$t0
	
	li	$t2,	90
	sw	$t2,	ANGLE
	sw	$zero,	ANGLE_CONTROL
	
	beq	$t0,	$zero,	ydone
	blt	$t0,	$zero,	yneg
	j	ypos

ypos:
	li	$t2,	SPEEDP
	sw	$t2,	VELOCITY
	sub	$a1,	$a1,	5
ypr:
	lw	$t0,	BOT_Y
	ble	$t0,	$a1,	ypr
	sw	$zero,	VELOCITY
	j	ydone
	
yneg:
	li	$t2,	SPEEDN
	sw	$t2,	VELOCITY
	add	$a1,	$a1,	5
ynr:
	lw	$t0,	BOT_Y
	bge	$t0,	$a1,	ynr
	sw	$zero,	VELOCITY
	j	ydone

ydone:
	jr	$ra
.data

puzzle: .space 4096
solution: .space 328
puzzle_ready: .space 1

.text

.globl request_resource
request_resource:
	sw	$zero,	puzzle_ready
	sw	$a0,	SET_RESOURCE_TYPE
	la	$t0,	puzzle
	sw	$t0,	REQUEST_PUZZLE
	la	$t0,	solution			# Zero the solution struct
	add	$t1,	$t0,	328
rrloop:
	beq	$t0,	$t1,	rrclear
	sw	$zero,	0($t0)
	add	$t0,	$t0,	4
	j	rrloop
rrclear:
	jr	$ra

stall_solve:
	lw	$t0,	puzzle_ready
	beq	$t0,	$zero,	stall_solve
	j	really_solve

.globl solve
solve:
	lw	$t0,	puzzle_ready
	bne	$t0,	$zero,	really_solve
	jr	$ra
really_solve:
	la	$a0,	solution			# The real work happens in recursive_backtracking from Lab 8
	la	$a1,	puzzle
	
	sub	$sp,	$sp,	4
	sw	$ra,	0($sp)
	
	jal	recursive_backtracking
	
	lw	$ra,	0($sp)
	add	$sp,	$sp,	4
	
	sw	$zero,	puzzle_ready
	la	$t0,	solution
	sw	$t0,	SUBMIT_SOLUTION
	
	jr	$ra
.data

delta_x:
.align	2
.word	1	-1

what_to_get:
.align	2
.word	1

need_something:
.align	2
.word	0

								# $s0 is current X position of planting
								# $s1 is delta-X for initial planting
								# $s2 is current Y position of planting
								# $s3 is final X for planting

.text

water_request_done:
	sw	$s6,	need_something
	sw	$zero,	what_to_get
	j	water_done

need_seeds:
	sw	$s6,	need_something
	sw	$s6,	what_to_get
	j	do_puzzle

.globl main
main:
	j	do_special_stuff
main_nospecial:
	jal	init_stack
	jal	fix_interrupts
	li	$a0,	1
	jal	request_resource

main_reset:
	
	li	$a0,	4
	li	$a1,	5
	jal	move_to
	
	lw	$t0,	BOT_X
	lw	$t1,	OTHER_BOT_X
	slt	$s0,	$t1,	$t0
	mul	$s1,	$s0,	4
	lw	$s1,	delta_x($s1)	# Hacky way to set 1 or -1 based on SLT result
	mul	$s0,	$s0,	9
	sub	$s3,	$zero,	$s0
	add	$s3,	$s3,	9
	
	lw	$t0,	BOT_Y
	lw	$t1,	OTHER_BOT_Y
	slt	$s2,	$t1,	$t0
	mul	$s2,	$s2,	7
	
								# So at this point, we've chosen a quadrant which is closer to us than the opponent.
								# The values are either (0, 1, 0, 9), (0, 1, 7, 9), (9, -1, 0, 0), or (9, -1, 7, 0).
								# We have enough starting seeds and water for nine plants, then we need to do puzzles.
	
	li	$s6,	1
	add	$s7,	$s2,	1
	add	$s5,	$s2,	2
	li	$s4,	2
	
mainplantloop:
	
#	beq	$s0,	$s3,	doneplant
	beq	$s0,	$s3,	main_reset	# Generate a new position and everything, in case our opponent has defeated us completely here
	lw	$t6,	GET_NUM_SEEDS
	ble	$t6,	$s4,	need_seeds
	
	lw	$t5,	GET_NUM_WATER_DROPS
	beq	$t5,	$zero,	water_request_done
	and	$t5,	$s0,	1
	beq	$t5,	$zero,	water_edges
water_center:
	sw	$s4,	0($t9)
	sw	$s0,	4($t9)
	sw	$s7,	8($t9)
	add	$t9,	$t9,	12
	j	water_done
water_edges:
	sw	$s4,	0($t9)
	sw	$s0,	4($t9)
	sw	$s2,	8($t9)
	sw	$s4,	12($t9)
	sw	$s0,	16($t9)
	sw	$s5,	20($t9)
	add	$t9,	$t9,	24
water_done:
	
	sw	$s6,	0($t9)
	sw	$s0,	4($t9)
	sw	$s2,	8($t9)
	
	sw	$s6,	12($t9)
	sw	$s0,	16($t9)
	sw	$s7,	20($t9)
	
	sw	$s6,	24($t9)
	sw	$s0,	28($t9)
	sw	$s5,	32($t9)
	
	add	$t9,	$t9,	36
	
	add	$s0,	$s0,	$s1
	
	jal	check_stack
	
	lw	$t8,	need_something
	bne	$t8,	$zero,	do_puzzle
	
	j	mainplantloop
	
do_puzzle:
	jal	stall_solve
	
	sw	$zero,	need_something
	
	lw	$a0,	what_to_get
	jal	request_resource
	
	jal	check_stack
	
	j	mainplantloop

doneplant:
	mul	$t4,	$s1,	9
	sub	$s0,	$s0,	$t4
	j	mainplantloop
.text

## int convert_highest_bit_to_int(int domain) {
##   int result = 0;
##   for (; domain; domain >>= 1) {
##     result ++;
##   }
##   return result;
## }

.globl convert_highest_bit_to_int
convert_highest_bit_to_int:
    move  $v0, $0   	      # result = 0

chbti_loop:
    beq   $a0, $0, chbti_end
    add   $v0, $v0, 1         # result ++
    sra   $a0, $a0, 1         # domain >>= 1
    j     chbti_loop

chbti_end:
    jr	  $ra
.text

## struct Cage {
##   char operation;
##   int target;
##   int num_cell;
##   int* positions;
## };
##
## struct Cell {
##   int domain;
##   Cage* cage;
## };
##
## struct Puzzle {
##   int size;
##   Cell* grid;
## };
##
## // Given the assignment at current position, removes all inconsistent values
## // for cells in the same row, column, and cage.
## int forward_checking(int position, Puzzle* puzzle) {
##   int size = puzzle->size;
##   // Removes inconsistent values in the row.
##   for (int col = 0; col < size; col++) {
##     if (col != position % size) {
##       puzzle->grid[position / size * size + col].domain &=
##           ~ puzzle->grid[position].domain;
##       if (!puzzle->grid[position / size * size + col].domain) {
##         return 0;
##       }
##     }
##   }
##   // Removes inconsistent values in the column.
##   for (int row = 0; row < size; row++) {
##     if (row != position / size) {
##       puzzle->grid[row * size + position % size].domain &=
##           ~ puzzle->grid[position].domain;
##       if (!puzzle->grid[row * size + position % size].domain) {
##         return 0;
##       }
##     }
##   }
##   // Removes inconsistent values in the cage.
##   for (int i = 0; i < puzzle->grid[position].cage->num_cell; i++) {
##     int pos = puzzle->grid[position].cage->positions[i];
##     puzzle->grid[pos].domain &= get_domain_for_cell(pos, puzzle);
##     if (!puzzle->grid[pos].domain) {
##       return 0;
##     }
##   }
##   return 1;
## }

.globl forward_checking
forward_checking:
  sub   $sp, $sp, 24
  sw    $ra, 0($sp)
  sw    $a0, 4($sp)
  sw    $a1, 8($sp)
  sw    $s0, 12($sp)
  sw    $s1, 16($sp)
  sw    $s2, 20($sp)
  lw    $t0, 0($a1)     # size
  li    $t1, 0          # col = 0
fc_for_col:
  bge   $t1, $t0, fc_end_for_col  # col < size
  div   $a0, $t0
  mfhi  $t2             # position % size
  mflo  $t3             # position / size
  beq   $t1, $t2, fc_for_col_continue    # if (col != position % size)
  mul   $t4, $t3, $t0
  add   $t4, $t4, $t1   # position / size * size + col
  mul   $t4, $t4, 8
  lw    $t5, 4($a1) # puzzle->grid
  add   $t4, $t4, $t5   # &puzzle->grid[position / size * size + col].domain
  mul   $t2, $a0, 8   # position * 8
  add   $t2, $t5, $t2 # puzzle->grid[position]
  lw    $t2, 0($t2) # puzzle -> grid[position].domain
  not   $t2, $t2        # ~puzzle->grid[position].domain
  lw    $t3, 0($t4) #
  and   $t3, $t3, $t2
  sw    $t3, 0($t4)
  beq   $t3, $0, fc_return_zero # if (!puzzle->grid[position / size * size + col].domain)
fc_for_col_continue:
  add   $t1, $t1, 1     # col++
  j     fc_for_col
fc_end_for_col:
  li    $t1, 0          # row = 0
fc_for_row:
  bge   $t1, $t0, fc_end_for_row  # row < size
  div   $a0, $t0
  mflo  $t2             # position / size
  mfhi  $t3             # position % size
  beq   $t1, $t2, fc_for_row_continue
  lw    $t2, 4($a1)     # puzzle->grid
  mul   $t4, $t1, $t0
  add   $t4, $t4, $t3
  mul   $t4, $t4, 8
  add   $t4, $t2, $t4   # &puzzle->grid[row * size + position % size]
  lw    $t6, 0($t4)
  mul   $t5, $a0, 8
  add   $t5, $t2, $t5
  lw    $t5, 0($t5)     # puzzle->grid[position].domain
  not   $t5, $t5
  and   $t5, $t6, $t5
  sw    $t5, 0($t4)
  beq   $t5, $0, fc_return_zero
fc_for_row_continue:
  add   $t1, $t1, 1     # row++
  j     fc_for_row
fc_end_for_row:

  li    $s0, 0          # i = 0
fc_for_i:
  lw    $t2, 4($a1)
  mul   $t3, $a0, 8
  add   $t2, $t2, $t3
  lw    $t2, 4($t2)     # &puzzle->grid[position].cage
  lw    $t3, 8($t2)     # puzzle->grid[position].cage->num_cell
  bge   $s0, $t3, fc_return_one
  lw    $t3, 12($t2)    # puzzle->grid[position].cage->positions
  mul   $s1, $s0, 4
  add   $t3, $t3, $s1
  lw    $t3, 0($t3)     # pos
  lw    $s1, 4($a1)
  mul   $s2, $t3, 8
  add   $s2, $s1, $s2   # &puzzle->grid[pos].domain
  lw    $s1, 0($s2)
  move  $a0, $t3
  jal get_domain_for_cell
  lw    $a0, 4($sp)
  lw    $a1, 8($sp)
  and   $s1, $s1, $v0
  sw    $s1, 0($s2)     # puzzle->grid[pos].domain &= get_domain_for_cell(pos, puzzle)
  beq   $s1, $0, fc_return_zero
fc_for_i_continue:
  add   $s0, $s0, 1     # i++
  j     fc_for_i
fc_return_one:
  li    $v0, 1
  j     fc_return
fc_return_zero:
  li    $v0, 0
fc_return:
  lw    $ra, 0($sp)
  lw    $a0, 4($sp)
  lw    $a1, 8($sp)
  lw    $s0, 12($sp)
  lw    $s1, 16($sp)
  lw    $s2, 20($sp)
  add   $sp, $sp, 24
  jr    $ra
.text

## int get_domain_for_addition(int target, int num_cell, int domain) {
##   int upper_bound = convert_highest_bit_to_int(domain);
##
##   // For an integer i, i & -i keeps only the lowest 1 in the integer.
##   int lower_bound = convert_highest_bit_to_int(domain & (-domain));
##
##   int high_bits = target - (num_cell - 1) * lower_bound;
##   if (high_bits < 0) {
##     high_bits = 0;
##   }
##   if (high_bits < upper_bound) {
##     domain = domain & ((1 << high_bits) - 1);
##   }
##
##   int low_bits = target - (num_cell - 1) * upper_bound;
##   if (low_bits > 0) {
##     domain = domain >> (low_bits - 1) << (low_bits - 1);
##   }
##
##   return domain;
## }

.globl get_domain_for_addition
get_domain_for_addition:
    sub    $sp, $sp, 20
    sw     $ra, 0($sp)
    sw     $s0, 4($sp)
    sw     $s1, 8($sp)
    sw     $s2, 12($sp)
    sw     $s3, 16($sp)
    move   $s0, $a0                     # s0 = target
    move   $s1, $a1                     # s1 = num_cell
    move   $s2, $a2                     # s2 = domain

    move   $a0, $a2
    jal    convert_highest_bit_to_int
    move   $s3, $v0                     # s3 = upper_bound

    sub    $a0, $0, $s2	                # -domain
    and    $a0, $a0, $s2                # domain & (-domain)
    jal    convert_highest_bit_to_int   # v0 = lower_bound
	   
    sub    $t0, $s1, 1                  # num_cell - 1
    mul    $t0, $t0, $v0                # (num_cell - 1) * lower_bound
    sub    $t0, $s0, $t0                # t0 = high_bits
    bge    $t0, 0, gdfa_skip0

    li     $t0, 0

gdfa_skip0:
    bge    $t0, $s3, gdfa_skip1

    li     $t1, 1          
    sll    $t0, $t1, $t0                # 1 << high_bits
    sub    $t0, $t0, 1                  # (1 << high_bits) - 1
    and    $s2, $s2, $t0                # domain & ((1 << high_bits) - 1)

gdfa_skip1:	   
    sub    $t0, $s1, 1                  # num_cell - 1
    mul    $t0, $t0, $s3                # (num_cell - 1) * upper_bound
    sub    $t0, $s0, $t0                # t0 = low_bits
    ble    $t0, $0, gdfa_skip2

    sub    $t0, $t0, 1                  # low_bits - 1
    sra    $s2, $s2, $t0                # domain >> (low_bits - 1)
    sll    $s2, $s2, $t0                # domain >> (low_bits - 1) << (low_bits - 1)

gdfa_skip2:	   
    move   $v0, $s2                     # return domain
    lw     $ra, 0($sp)
    lw     $s0, 4($sp)
    lw     $s1, 8($sp)
    lw     $s2, 12($sp)
    lw     $s3, 16($sp)
    add    $sp, $sp, 20
    jr     $ra
.text

## int get_domain_for_subtraction(int target, int domain, int other_domain) {
##   int base_mask = 1 | (1 << (target * 2));
##   int mask = 0;
##   for (; other_domain; other_domain >>= 1) {
##     if (other_domain & 1) {
##       mask |= (base_mask >> target);
##     }
##     base_mask <<= 1;
##   }
##   return domain & mask;
## }

.globl get_domain_for_subtraction
get_domain_for_subtraction:
    li     $t0, 1              
    li     $t1, 2
    mul    $t1, $t1, $a0            # target * 2
    sll    $t1, $t0, $t1            # 1 << (target * 2)
    or     $t0, $t0, $t1            # t0 = base_mask
    li     $t1, 0                   # t1 = mask

gdfs_loop:
    beq    $a2, $0, gdfs_loop_end	
    and    $t2, $a2, 1              # other_domain & 1
    beq    $t2, $0, gdfs_if_end
	   
    sra    $t2, $t0, $a0            # base_mask >> target
    or     $t1, $t1, $t2            # mask |= (base_mask >> target)

gdfs_if_end:
    sll    $t0, $t0, 1              # base_mask <<= 1
    sra    $a2, $a2, 1              # other_domain >>= 1
    j      gdfs_loop

gdfs_loop_end:
    and    $v0, $a1, $t1            # domain & mask
    jr	   $ra
.text

## struct Puzzle {
##   int size;
##   Cell* grid;
## };
##
## struct Solution {
##   int size;
##   int assignment[81];
## };
##
## // Returns next position for assignment.
## int get_unassigned_position(const Solution* solution, const Puzzle* puzzle) {
##   int unassigned_pos = 0;
##   for (; unassigned_pos < puzzle->size * puzzle->size; unassigned_pos++) {
##     if (solution->assignment[unassigned_pos] == 0) {
##       break;
##     }
##   }
##   return unassigned_pos;
## }

.globl get_unassigned_position
get_unassigned_position:
  li    $v0, 0            # unassigned_pos = 0
  lw    $t0, 0($a1)       # puzzle->size
  mul  $t0, $t0, $t0     # puzzle->size * puzzle->size
  add   $t1, $a0, 4       # &solution->assignment[0]
get_unassigned_position_for_begin:
  bge   $v0, $t0, get_unassigned_position_return  # if (unassigned_pos < puzzle->size * puzzle->size)
  mul  $t2, $v0, 4
  add   $t2, $t1, $t2     # &solution->assignment[unassigned_pos]
  lw    $t2, 0($t2)       # solution->assignment[unassigned_pos]
  beq   $t2, 0, get_unassigned_position_return  # if (solution->assignment[unassigned_pos] == 0)
  add   $v0, $v0, 1       # unassigned_pos++
  j   get_unassigned_position_for_begin
get_unassigned_position_return:
  jr    $ra
.text

## struct Puzzle {
##   int size;
##   Cell* grid;
## };
##
## struct Solution {
##   int size;
##   int assignment[81];
## };
##
## // Checks if the solution is complete.
## int is_complete(const Solution* solution, const Puzzle* puzzle) {
##   return solution->size == puzzle->size * puzzle->size;
## }

.globl is_complete
is_complete:
  lw    $t0, 0($a0)       # solution->size
  lw    $t1, 0($a1)       # puzzle->size
  mul   $t1, $t1, $t1     # puzzle->size * puzzle->size
  move	$v0, $0
  seq   $v0, $t0, $t1
  j     $ra
.text

## int is_single_value_domain(int domain) {
##   if (domain != 0 && (domain & (domain - 1)) == 0) {
##     return 1;
##   }
##   return 0;
## }

.globl is_single_value_domain
is_single_value_domain:
    beq    $a0, $0, isvd_zero     # return 0 if domain == 0
    sub    $t0, $a0, 1	          # (domain - 1)
    and    $t0, $t0, $a0          # (domain & (domain - 1))
    bne    $t0, $0, isvd_zero     # return 0 if (domain & (domain - 1)) != 0
    li     $v0, 1
    jr	   $ra

isvd_zero:	   
    li	   $v0, 0
    jr	   $ra
.text

## struct Cage {
##   char operation;
##   int target;
##   int num_cell;
##   int* positions;
## };
##
## struct Cell {
##   int domain;
##   Cage* cage;
## };
##
## struct Puzzle {
##   int size;
##   Cell* grid;
## };
##
## struct Solution {
##   int size;
##   int assignment[81];
## };
##
## int recursive_backtracking(Solution* solution, Puzzle* puzzle) {
##   if (is_complete(solution, puzzle)) {
##     return 1;
##   }
##   int position = get_unassigned_position(solution, puzzle);
##   for (int val = 1; val < puzzle->size + 1; val++) {
##     if (puzzle->grid[position].domain & (0x1 << (val - 1))) {
##       solution->assignment[position] = val;
##       solution->size += 1;
##       // Applies inference to reduce space of possible assignment.
##       Puzzle puzzle_copy;
##       Cell grid_copy [81]; // 81 is the maximum size of the grid.
##       puzzle_copy.grid = grid_copy;
##       clone(puzzle, &puzzle_copy);
##       puzzle_copy.grid[position].domain = 0x1 << (val - 1);
##       if (forward_checking(position, &puzzle_copy)) {
##         if (recursive_backtracking(solution, &puzzle_copy)) {
##           return 1;
##         }
##       }
##       solution->assignment[position] = 0;
##       solution->size -= 1;
##     }
##   }
##   return 0;
## }

.globl recursive_backtracking
recursive_backtracking:
  sub   $sp, $sp, 680
  sw    $ra, 0($sp)
  sw    $a0, 4($sp)     # solution
  sw    $a1, 8($sp)     # puzzle
  sw    $s0, 12($sp)    # position
  sw    $s1, 16($sp)    # val
  sw    $s2, 20($sp)    # 0x1 << (val - 1)
                        # sizeof(Puzzle) = 8
                        # sizeof(Cell [81]) = 648

#	jal	check_stack		# Stack check in the recursion! ADDED BY DANIEL - NOT IN PROVIDED FILE
#	lw	$ra,	0($sp)

  jal   is_complete
  bne   $v0, $0, recursive_backtracking_return_one
  lw    $a0, 4($sp)     # solution
  lw    $a1, 8($sp)     # puzzle
  jal   get_unassigned_position
  move  $s0, $v0        # position
  li    $s1, 1          # val = 1
recursive_backtracking_for_loop:
  lw    $a0, 4($sp)     # solution
  lw    $a1, 8($sp)     # puzzle
  lw    $t0, 0($a1)     # puzzle->size
  add   $t1, $t0, 1     # puzzle->size + 1
  bge   $s1, $t1, recursive_backtracking_return_zero  # val < puzzle->size + 1
  lw    $t1, 4($a1)     # puzzle->grid
  mul   $t4, $s0, 8     # sizeof(Cell) = 8
  add   $t1, $t1, $t4   # &puzzle->grid[position]
  lw    $t1, 0($t1)     # puzzle->grid[position].domain
  sub   $t4, $s1, 1     # val - 1
  li    $t5, 1
  sll   $s2, $t5, $t4   # 0x1 << (val - 1)
  and   $t1, $t1, $s2   # puzzle->grid[position].domain & (0x1 << (val - 1))
  beq   $t1, $0, recursive_backtracking_for_loop_continue # if (domain & (0x1 << (val - 1)))
  mul   $t0, $s0, 4     # position * 4
  add   $t0, $t0, $a0
  add   $t0, $t0, 4     # &solution->assignment[position]
  sw    $s1, 0($t0)     # solution->assignment[position] = val
  lw    $t0, 0($a0)     # solution->size
  add   $t0, $t0, 1
  sw    $t0, 0($a0)     # solution->size++
  add   $t0, $sp, 32    # &grid_copy
  sw    $t0, 28($sp)    # puzzle_copy.grid = grid_copy !!!
  move  $a0, $a1        # &puzzle
  add   $a1, $sp, 24    # &puzzle_copy
  jal   clone           # clone(puzzle, &puzzle_copy)
  mul   $t0, $s0, 8     # !!! grid size 8
  lw    $t1, 28($sp)
  
  add   $t1, $t1, $t0   # &puzzle_copy.grid[position]
  sw    $s2, 0($t1)     # puzzle_copy.grid[position].domain = 0x1 << (val - 1);
  move  $a0, $s0
  add   $a1, $sp, 24
  jal   forward_checking  # forward_checking(position, &puzzle_copy)
  beq   $v0, $0, recursive_backtracking_skip

  lw    $a0, 4($sp)     # solution
  add   $a1, $sp, 24    # &puzzle_copy
  jal   recursive_backtracking
  beq   $v0, $0, recursive_backtracking_skip
  j     recursive_backtracking_return_one # if (recursive_backtracking(solution, &puzzle_copy))
recursive_backtracking_skip:
  lw    $a0, 4($sp)     # solution
  mul   $t0, $s0, 4
  add   $t1, $a0, 4
  add   $t1, $t1, $t0
  sw    $0, 0($t1)      # solution->assignment[position] = 0
  lw    $t0, 0($a0)
  sub   $t0, $t0, 1
  sw    $t0, 0($a0)     # solution->size -= 1
recursive_backtracking_for_loop_continue:
  add   $s1, $s1, 1     # val++
  j     recursive_backtracking_for_loop
recursive_backtracking_return_zero:
  li    $v0, 0
  j     recursive_backtracking_return
recursive_backtracking_return_one:
  li    $v0, 1
recursive_backtracking_return:
  lw    $ra, 0($sp)
  lw    $a0, 4($sp)
  lw    $a1, 8($sp)
  lw    $s0, 12($sp)
  lw    $s1, 16($sp)
  lw    $s2, 20($sp)
  add   $sp, $sp, 680
  jr    $ra
.text

.globl clone
clone:

    lw  $t0, 0($a0)
    sw  $t0, 0($a1)

    mul $t0, $t0, $t0
    mul $t0, $t0, 2 # two words in one grid

    lw  $t1, 4($a0) # &puzzle(ori).grid
    lw  $t2, 4($a1) # &puzzle(clone).grid

    li  $t3, 0 # i = 0;
clone_for_loop:
    bge  $t3, $t0, clone_for_loop_end
    sll $t4, $t3, 2 # i * 4
    add $t5, $t1, $t4 # puzzle(ori).grid ith word
    lw   $t6, 0($t5)

    add $t5, $t2, $t4 # puzzle(clone).grid ith word
    sw   $t6, 0($t5)
    
    addi $t3, $t3, 1 # i++
    
    j    clone_for_loop
clone_for_loop_end:

    jr  $ra
.text

.globl get_domain_for_cell
get_domain_for_cell:
    # save registers    
    sub $sp, $sp, 36
    sw $ra, 0($sp)
    sw $s0, 4($sp)
    sw $s1, 8($sp)
    sw $s2, 12($sp)
    sw $s3, 16($sp)
    sw $s4, 20($sp)
    sw $s5, 24($sp)
    sw $s6, 28($sp)
    sw $s7, 32($sp)

    li $t0, 0 # valid_domain
    lw $t1, 4($a1) # puzzle->grid (t1 free)
    sll $t2, $a0, 3 # position*8 (actual offset) (t2 free)
    add $t3, $t1, $t2 # &puzzle->grid[position]
    lw  $t4, 4($t3) # &puzzle->grid[position].cage
    lw  $t5, 0($t4) # puzzle->grid[posiition].cage->operation

    lw $t2, 4($t4) # puzzle->grid[position].cage->target

    move $s0, $t2   # remain_target = $s0  *!*!
    lw $s1, 8($t4) # remain_cell = $s1 = puzzle->grid[position].cage->num_cell
    lw $s2, 0($t3) # domain_union = $s2 = puzzle->grid[position].domain
    move $s3, $t4 # puzzle->grid[position].cage
    li $s4, 0   # i = 0
    move $s5, $t1 # $s5 = puzzle->grid
    move $s6, $a0 # $s6 = position
    # move $s7, $s2 # $s7 = puzzle->grid[position].domain

    bne $t5, 0, gdfc_check_else_if

    li $t1, 1
    sub $t2, $t2, $t1 # (puzzle->grid[position].cage->target-1)
    sll $v0, $t1, $t2 # valid_domain = 0x1 << (prev line comment)
    j gdfc_end # somewhere!!!!!!!!

gdfc_check_else_if:
    bne $t5, '+', gdfc_check_else

gdfc_else_if_loop:
    lw $t5, 8($s3) # puzzle->grid[position].cage->num_cell
    bge $s4, $t5, gdfc_for_end # branch if i >= puzzle->grid[position].cage->num_cell
    sll $t1, $s4, 2 # i*4
    lw $t6, 12($s3) # puzzle->grid[position].cage->positions
    add $t1, $t6, $t1 # &puzzle->grid[position].cage->positions[i]
    lw $t1, 0($t1) # pos = puzzle->grid[position].cage->positions[i]
    add $s4, $s4, 1 # i++

    sll $t2, $t1, 3 # pos * 8
    add $s7, $s5, $t2 # &puzzle->grid[pos]
    lw  $s7, 0($s7) # puzzle->grid[pos].domain

    beq $t1, $s6 gdfc_else_if_else # branch if pos == position

    

    move $a0, $s7 # $a0 = puzzle->grid[pos].domain
    jal is_single_value_domain
    bne $v0, 1 gdfc_else_if_else # branch if !is_single_value_domain()
    move $a0, $s7
    jal convert_highest_bit_to_int
    sub $s0, $s0, $v0 # remain_target -= convert_highest_bit_to_int
    addi $s1, $s1, -1 # remain_cell -= 1
    j gdfc_else_if_loop
gdfc_else_if_else:
    or $s2, $s2, $s7 # domain_union |= puzzle->grid[pos].domain
    j gdfc_else_if_loop

gdfc_for_end:
    move $a0, $s0
    move $a1, $s1
    move $a2, $s2
    jal get_domain_for_addition # $v0 = valid_domain = get_domain_for_addition()
    j gdfc_end

gdfc_check_else:
    lw $t3, 12($s3) # puzzle->grid[position].cage->positions
    lw $t0, 0($t3) # puzzle->grid[position].cage->positions[0]
    lw $t1, 4($t3) # puzzle->grid[position].cage->positions[1]
    xor $t0, $t0, $t1
    xor $t0, $t0, $s6 # other_pos = $t0 = $t0 ^ position
    lw $a0, 4($s3) # puzzle->grid[position].cage->target

    sll $t2, $s6, 3 # position * 8
    add $a1, $s5, $t2 # &puzzle->grid[position]
    lw  $a1, 0($a1) # puzzle->grid[position].domain
    # move $a1, $s7 

    sll $t1, $t0, 3 # other_pos*8 (actual offset)
    add $t3, $s5, $t1 # &puzzle->grid[other_pos]
    lw $a2, 0($t3)  # puzzle->grid[other_pos].domian

    jal get_domain_for_subtraction # $v0 = valid_domain = get_domain_for_subtraction()
    # j gdfc_end
gdfc_end:
# restore registers
    
    lw $ra, 0($sp)
    lw $s0, 4($sp)
    lw $s1, 8($sp)
    lw $s2, 12($sp)
    lw $s3, 16($sp)
    lw $s4, 20($sp)
    lw $s5, 24($sp)
    lw $s6, 28($sp)
    lw $s7, 32($sp)
    add $sp, $sp, 36    
    jr $ra
.text
do_special_stuff:
	j main_nospecial
