#!/bin/bash
cat provided/convert_highest_bit_to_int.s provided/forward_checking.s provided/get_domain_for_addition.s provided/get_domain_for_subtraction.s provided/get_unassigned_position.s provided/is_complete.s provided/is_single_value_domain.s provided/recursive_backtracking.s provided/clone.s provided/get_domain_for_cell.s > provided.s
cat constants.s interrupts.s movement.s puzzle_solver.s experimental_main.s provided.s bot1.s > spimbot.s
cat constants.s interrupts.s movement.s puzzle_solver.s experimental_main.s provided.s bot2.s > spimbot2.s
