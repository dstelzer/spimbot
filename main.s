.data
.align 2
tile_data: .space 1600


.text

.globl main
main:
	jal	init_stack					# Set up the custom stack
	jal	fix_interrupts
	li	$a0,	0					# Request something
	jal	request_resource
	jal	check_stack

	li	$t1, 2
	li	$t2, 1
	li	$t7, 0
	li	$t8, 0
tmp:
	la	$t0, tile_data
	sw	$t0, TILE_SCAN

	bne	$t7,	3,	next	#once x hits 3 we reset x to zero and add one to y
	li	$t7,	0
	add	$t8,	1,	1
next:
	bne	$t8,	4,	next2	#once y hits four we reset x and y to
	li	$t7,	0
	li	$t8,	0
next2:

					##check state of current location (t7, t8) not exactly sure how to do this
					##set flag ($t5) to 0 if state == 0

	beq	$t5,	0, no_plant
plant:
	sw	$t2,	0($t9)
	sw	$t7,	4($t9)
	sw	$t8,	8($t9)
	add	$t9,	$t9,	12
water:
	add	$t2,	$t2,	1
	sw	$t2,	0($t9)
	sw	$t7,	4($t9)
	sw	$t8,	8($t9)
	add	$t9,	$t9,	12	

no_plant:
	jal	stall_solve
	jal	check_stack
	li	$a0,	1
	jal	request_resource
	add	$t7,	$t7,	1
	
	
	j	tmp

