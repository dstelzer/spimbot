.data

delta_x:
.align	2
.word	1	-1

what_to_get:
.align	2
.word	1

need_something:
.align	2
.word	0

								# $s0 is current X position of planting
								# $s1 is delta-X for initial planting
								# $s2 is current Y position of planting
								# $s3 is final X for planting

.text

water_request_done:
	sw	$s6,	need_something
	sw	$zero,	what_to_get
	j	water_done

need_seeds:
	sw	$s6,	need_something
	sw	$s6,	what_to_get
	j	do_puzzle

.globl main
main:
	j	do_special_stuff
main_nospecial:
	jal	init_stack
	jal	fix_interrupts
	li	$a0,	1
	jal	request_resource

main_reset:
	
	li	$a0,	4
	li	$a1,	5
	jal	move_to
	
	lw	$t0,	BOT_X
	lw	$t1,	OTHER_BOT_X
	slt	$s0,	$t1,	$t0
	mul	$s1,	$s0,	4
	lw	$s1,	delta_x($s1)	# Hacky way to set 1 or -1 based on SLT result
	mul	$s0,	$s0,	9
	sub	$s3,	$zero,	$s0
	add	$s3,	$s3,	9
	
	lw	$t0,	BOT_Y
	lw	$t1,	OTHER_BOT_Y
	slt	$s2,	$t1,	$t0
	mul	$s2,	$s2,	7
	
								# So at this point, we've chosen a quadrant which is closer to us than the opponent.
								# The values are either (0, 1, 0, 9), (0, 1, 7, 9), (9, -1, 0, 0), or (9, -1, 7, 0).
								# We have enough starting seeds and water for nine plants, then we need to do puzzles.
	
	li	$s6,	1
	add	$s7,	$s2,	1
	add	$s5,	$s2,	2
	li	$s4,	2
	
mainplantloop:
	
#	beq	$s0,	$s3,	doneplant
	beq	$s0,	$s3,	main_reset	# Generate a new position and everything, in case our opponent has defeated us completely here
	lw	$t6,	GET_NUM_SEEDS
	ble	$t6,	$s4,	need_seeds
	
	lw	$t5,	GET_NUM_WATER_DROPS
	beq	$t5,	$zero,	water_request_done
	and	$t5,	$s0,	1
	beq	$t5,	$zero,	water_edges
water_center:
	sw	$s4,	0($t9)
	sw	$s0,	4($t9)
	sw	$s7,	8($t9)
	add	$t9,	$t9,	12
	j	water_done
water_edges:
	sw	$s4,	0($t9)
	sw	$s0,	4($t9)
	sw	$s2,	8($t9)
	sw	$s4,	12($t9)
	sw	$s0,	16($t9)
	sw	$s5,	20($t9)
	add	$t9,	$t9,	24
water_done:
	
	sw	$s6,	0($t9)
	sw	$s0,	4($t9)
	sw	$s2,	8($t9)
	
	sw	$s6,	12($t9)
	sw	$s0,	16($t9)
	sw	$s7,	20($t9)
	
	sw	$s6,	24($t9)
	sw	$s0,	28($t9)
	sw	$s5,	32($t9)
	
	add	$t9,	$t9,	36
	
	add	$s0,	$s0,	$s1
	
	jal	check_stack
	
	lw	$t8,	need_something
	bne	$t8,	$zero,	do_puzzle
	
	j	mainplantloop
	
do_puzzle:
	jal	stall_solve
	
	sw	$zero,	need_something
	
	lw	$a0,	what_to_get
	jal	request_resource
	
	jal	check_stack
	
	j	mainplantloop

doneplant:
	mul	$t4,	$s1,	9
	sub	$s0,	$s0,	$t4
	j	mainplantloop
