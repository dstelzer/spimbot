.text

fix_interrupts:
	li	$t4,	ON_FIRE_MASK						# timer interrupt enable bit
	or	$t4,	$t4,	MAX_GROWTH_INT_MASK			# timer interrupt enable bit
	or	$t4,	$t4,	REQUEST_PUZZLE_INT_MASK		# timer interrupt enable bit
	or	$t4,	$t4,	1							# global interrupt enable
	mtc0	$t4, $12								# set interrupt mask (Status register)
	jr	$ra

.kdata				# interrupt handler data (separated just for readability)
.align		2
chunkIH:	.space 8	# space for two registers
non_intrpt_str:	.asciiz "Non-interrupt exception\n"
unhandled_str:	.asciiz "Unhandled interrupt type\n"

.ktext 0x80000180
interrupt_handler:
.set noat
	move	$k1, $at		# Save $at
.set at
	la	$k0, chunkIH
	sw	$a0, 0($k0)		# Get some free registers
	sw	$a1, 4($k0)		# by storing them to a global variable

	mfc0	$k0, $13		# Get Cause register
	srl	$a0, $k0, 2
	and	$a0, $a0, 0xf		# ExcCode field
	bne	$a0, 0, non_intrpt         

interrupt_dispatch:			# Interrupt
	mfc0	$k0, $13		# Get Cause register, again
	beq	$k0, 0, done		# handled all outstanding interrupts

	and	$a0, $k0, ON_FIRE_MASK
	bne	$a0, 0, fire_interrupt
	
	and	$a0, $k0, MAX_GROWTH_INT_MASK
	bne	$a0, 0, growth_interrupt
	
	and	$a0, $k0, REQUEST_PUZZLE_INT_MASK
	bne	$a0, 0, puzzle_interrupt

	li	$v0, PRINT_STRING	# Unhandled interrupt types
	la	$a0, unhandled_str
	syscall
	j	done

fire_interrupt:
	sw	$zero,	ON_FIRE_ACK		# acknowledge interrupt
	
	li	$a0,	3
	sw	$a0,	0($t9)

	lw	$a0,	GET_FIRE_LOC
	and	$a1,	$a0,	0xffff	# lower bits
	srl	$a0,	$a0,	16
	
	sw	$a0,	4($t9)
	sw	$a1,	8($t9)
	add	$t9,	$t9,	12

	j	interrupt_dispatch	# see if other interrupts are waiting

growth_interrupt:
	sw	$zero,	MAX_GROWTH_ACK		# acknowledge interrupt
	
	li	$a0,	4
	sw	$a0,	0($t9)

	lw	$a0,	MAX_GROWTH_TILE
	and	$a1,	$a0,	0xffff	# lower bits
	srl	$a0,	$a0,	16
	
	sw	$a0,	4($t9)
	sw	$a1,	8($t9)
	add	$t9,	$t9,	12

	j	interrupt_dispatch	# see if other interrupts are waiting

puzzle_interrupt:
	sw	$zero,	REQUEST_PUZZLE_ACK
	
	sw	$a0,	puzzle_ready
	j	interrupt_dispatch

non_intrpt:				# was some non-interrupt
	li	$v0, PRINT_STRING
	la	$a0, non_intrpt_str
	syscall				# print out an error message
	# fall through to done

done:
	la	$k0, chunkIH
	lw	$a0, 0($k0)		# Restore saved registers
	lw	$a1, 4($k0)
.set noat
	move	$at, $k1		# Restore $at
.set at 
	eret
