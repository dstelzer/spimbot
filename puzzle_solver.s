.data

puzzle: .space 4096
solution: .space 328
puzzle_ready: .space 1

.text

.globl request_resource
request_resource:
	sw	$zero,	puzzle_ready
	sw	$a0,	SET_RESOURCE_TYPE
	la	$t0,	puzzle
	sw	$t0,	REQUEST_PUZZLE
	la	$t0,	solution			# Zero the solution struct
	add	$t1,	$t0,	328
rrloop:
	beq	$t0,	$t1,	rrclear
	sw	$zero,	0($t0)
	add	$t0,	$t0,	4
	j	rrloop
rrclear:
	jr	$ra

stall_solve:
	lw	$t0,	puzzle_ready
	beq	$t0,	$zero,	stall_solve
	j	really_solve

.globl solve
solve:
	lw	$t0,	puzzle_ready
	bne	$t0,	$zero,	really_solve
	jr	$ra
really_solve:
	la	$a0,	solution			# The real work happens in recursive_backtracking from Lab 8
	la	$a1,	puzzle
	
	sub	$sp,	$sp,	4
	sw	$ra,	0($sp)
	
	jal	recursive_backtracking
	
	lw	$ra,	0($sp)
	add	$sp,	$sp,	4
	
	sw	$zero,	puzzle_ready
	la	$t0,	solution
	sw	$t0,	SUBMIT_SOLUTION
	
	jr	$ra
